'use strict';

var express = require('express');

var app = express();
var port = process.env.PORT || 8080;

var request = require('request');
var user = "devjob";
var pass = "poorlycodedpassword";
// var receiver = "+33695410018";
var receiver = "+41765363776";
var message = "Hi, my name is Adrien Bucci, you can find the code used to send this sms @ https://github.com/adrienbucci/netoxygen";

app.get('/', function (req, res) {
    request('https://sms.netoxygen.ch/url/?format=json&type=text&action=send&user=' + user + '&pass=' + pass + '&msg=' + message + '&dest=' + receiver,
        function (err, body) {
            if (body.statusCode == 200) {
                    console.log("Message envoyé avec succès, consultez votre smartphone svp.");
                    res.json("Message envoyé avec succès, consultez votre smartphone svp. Informations sevreur disponibles en console.");
                    console.log(body);
            }
        });
});

app.listen(port);
console.log('Server started on http://localhost:' + port);
